# version note
# modification of the way dpt is managed in twake class

# import functions
import matplotlib.pyplot as plt
# from jax import jit
from functools import partial
# from matplotlib import cm
import copy
plt.style.use('default')
from tqdm import tqdm
import scipy.constants as cons
# from scipy.integrate import solve_ivp, quad
from scipy.optimize import fsolve
# from scipy.interpolate import UnivariateSpline
import numpy as np
# import jax.numpy as jnp
import math
import sys, os, h5py, shutil, json
from pathlib import Path
# from shutil import copy2
# from astropy.table import QTable, Table, Column
# from astropy import units as u
# from astropy.io import ascii
# from datetime import date
from PyHEADTAIL.radiation.radiation import SynchrotronRadiationTransverse, SynchrotronRadiationLongitudinal
from PyHEADTAIL.particles.slicing import UniformBinSlicer
from PyHEADTAIL.impedances.wakes import WakeTable, WakeField
from PyHEADTAIL.trackers.transverse_tracking import TransverseMap
from PyHEADTAIL.trackers.longitudinal_tracking import RFSystems
from PyHEADTAIL.particles.generators import ParticleGenerator, gaussian2D, RF_bucket_distribution
from PyHEADTAIL.particles import generators
from PyHEADTAIL.trackers.detuners import Chromaticity
from PyHEADTAIL.feedback.transverse_damper import TransverseDamper
import PyHEADTAIL.general.pmath as pm

#
class beam_param:
    'Calculates different parameters at different modes for the booster '
    # initialization or constructor method of
    # parent_dir = str(Path(os.getcwd()).resolve().parents[1])
    parent_dir = os.path.dirname(os.getcwd())
    input_dir = parent_dir + '/input_data'
    input_files = []
    for file in os.listdir(input_dir):
        if file.endswith('.json'):
            input_files.append(file)
    inputs_ = []
    for filename in input_files:
        filename = input_dir + '/' + filename
        inputs_.append(json.load(open(filename)))
    def __init__(self, mode, freq, **kw):
        if mode == None:
            print('No mode entered -> default = inj90')
            mode = 'inj90'
        if freq == None:
            print('No frequency entered -> default = 800e+6 Hz')
            freq = 800e+6
        try :
            _defaults_ = next(item for item in beam_param.inputs_ if item["mode"] == mode)
            kw_loc = dict(_defaults_)
            kw_loc.update(kw)
            for key, val in kw_loc.items():
                setattr(self, key, val)
            self.set_freq(freq)
            self.set_alpha()
            self.V_()
            self.dp_()
            self.sigE2_eq()
        except:
            print('Error : mode '+ mode+ ' not found in input files')
    
    def sigE2_eq(self):
        T0 = self.length / cons.c
        self.tx = 1/((2*self.Cgamma*(self.Etot*1e-9)**3*self.I2)/(4.*np.pi*T0))
        self.sig_e_eq = np.sqrt(self.Cq*self.gamma**2*self.I3/(2*self.I2))#*(self.Etot*1e-9)**2)
        self.emit_eq = self.Cq * self.gamma**2 * self.I5 / self.I2 # geometrical equilibrium emittance
        self.epsnx_eq = self.emit_eq * self.gamma # normalized equilibrium emittance X
        self.epsny_eq = self.epsnx_eq * 2e-3 # normalized equilibrium emittance Y
    
    def set_freq(self, freq=None):
        if freq is not None:
            self.freq = freq
        self.lambdaRF = cons.c/self.freq # RF wavelength
        self.frev = cons.c/self.length
        self.h = self.freq/self.frev

    def set_alpha(self, alpha=None):
        if alpha is not None:
            self.alpha = alpha
        self.gammat = 1/np.sqrt(self.alpha) # transition energy
        self.gamma = self.Etot / self.Erest
        self.eta = 1/self.gamma**2-self.alpha # momentum compaction
        self.beta = np.sqrt(1-1/self.gamma**2) # normalized velocity
    
    def V_(self, sigma_e=None, sigma_z=None, Vt=None):
        if sigma_z != None :
            self.sigma_z = sigma_z
        if sigma_e  != None:
            self.sigma_e = sigma_e
        self.U0 = self.Cgamma*(self.Etot*1e-9)**4/2/np.pi*self.I2*1e9 # Synchrotron energy loss per turn
        self.Vtot = np.sqrt( (self.lambdaRF*self.eta*self.sigma_e**2*self.Etot*self.length/self.sigma_z**2/2/np.pi/self.beta**3)**2 + (self.Egain + self.U0)**2 ) # total voltage in V
        print(self.length, self.sigma_e, self.sigma_z, self.Etot, self.eta, self.freq, self.U0)
        # self.Vtot = np.sqrt( (self.length**2*self.sigma_e**2*self.Etot*self.eta/2/np.pi/self.freq/self.sigma_z**2/self.beta**3)**2 + (self.Egain + self.U0)**2)

    def dp_(self, Vx=None):
        if Vx is not None:
            self.Vx = Vx
        else:
            self.Vx = self.Vtot
        self.U0 = self.Cgamma*(self.Etot*1e-9)**4/2/np.pi*self.I2*1e9 # Synchrotron energy loss per turn
        self.phis = np.arcsin((self.Egain+self.U0)/self.Vx) # phase synchrone
        self.gradient = self.Vx/self.length # accelerating gradient
        if self.gamma > self.gammat:
            self.phis = np.pi-self.phis
        else:
            self.phis = phis
            self.Egain = self.Vtot*self.phis-self.U0
        fac = 2*self.beta**3*self.Etot*self.lambdaRF/np.pi/self.eta
        self.dEmax = np.sqrt((np.cos(self.phis)-(np.pi/2-self.phis)*np.sin(self.phis))*fac*self.gradient)
        self.dp = self.dEmax/self.Etot
        self.Qs = self.length/np.pi*np.sqrt(self.gradient/fac*np.cos(self.phis)) # synchrotron tune


def dp_vrf(x, *args, func=beam_param):
    """
    Code for voltage calculation with target momentum acceptance
    Credit : Barbara Dalena
    """
    dpt = args[0]
    f_rf = args[1]
    mode = args[2]
    F = func(mode=mode, freq=f_rf)
    Cg = F.Cgamma
    E0 = F.Etot
    I2 = F.I2
    C = F.length
    alpha_c = F.alpha
    U0 = F.U0
    frev = cons.c/C
    h = f_rf/frev
    phis = math.pi/2. - np.arccos(U0/x)
    Qs = np.sqrt(x/E0*(np.cos(phis)*alpha_c*h)/2/math.pi)
    return (2*Qs/(h*alpha_c))*np.sqrt((1+(phis-math.pi/2.)*np.tan(phis))) - dpt

class twake3_():
    ##
    _defaults_ = {
     ## MACHINE AND BEAM PARAMETERS
    "n_particles" : 3e10, # number of particles
    "mass" : cons.m_e, # mass
    "charge" : cons.e, # charge
    "mode" : "inj90",
    "p_increment" : 0.,
    "circumference" : 91.174117e3, # [m]
    "dphi_RF" : 0.,
    "longitudinal_mode" : 'non-linear',
    "sigma_dp" : 1e-3,
    "sigma_z" : None,
    "frequency" : 800e+6, # [MHz]
    # numerical parameters
    "n_turns" : 2,
    "n_macroparticles" : int(1e6),
    ## SLICING PARAMETERS
    "n_slices" : 500,
    "slicing_mode" : 'fixed_cuts', # from_first_to_last_particles, fixed_cuts
    "fixed_cuts_perc_min_max" : 0.50,
    # SAVING PARAMETERS
    "damp_slice_n_turns" : 500,
    "plot_every" : 500,
    "plot_every_finer" : 100,
    "compute_x_moments" : True,
    "compute_y_moments" : True,
    "compute_z_moments" : False,
    "max_x_mom_order" : 10, #10
    "max_y_mom_order" : 10, #10
    "max_z_mom_order" : 10, #10
    "save_bunch_at_end" : True,
    # SYNCHROTRON RADIATION PARAMETERS
    "damping_time_z_turns" : 15000, # SR damping time
    "wake" : True, # activate or deactivate wake
    "epsn_x" : None,#10e-6, # horizontal geometrical emittance should not be more than 10um
    "epsn_y" : None,#10e-6, # vertical geometrical emittance sweep from 1 to 10um
    "copper" : True, # if True, consider wake for copper beam-pipe, if False stainless steel
    "dir_name" : None, # if none then default folder name
    "dpt" : None,
    "V_RF" : None
    }
    ## 
    def __init__(self, d=None, **kw):
        kw_loc = dict(twake3_._defaults_)
        kw_loc.update(kw)
        for key, val in kw_loc.items():
            setattr(self, key, val)
        if d is not None:
            self.d = d
            dir_ = ''
            for key, val in d.items():
                setattr(self, key, val)
                if len(dir_) < 30 :
                    if len(dir_) != 0:
                        dir_ = dir_ + '_'
                    i0 = key
                    i1 = val
                    if i0 == 'frequency':
                        i1 = int(i1*1e-6)
                        i0 = 'fr'
                    elif i0 == 'sigma_z':
                        i1 = "{:.1e}".format(i1).replace('.','_').replace('0','')
                        i0 = 'sz'
                    elif i0 == 'n_particles':
                        i0 = 'np'
                    elif i0 == 'n_turns':
                        i1 = "{:.1e}".format(i1).replace('.','_').replace('0','')
                        i0 = 'nt'
                    elif i0 == 'wake':
                        i0 = 'wa'
                        i1 = int(i1)
                    elif len(i0)>3:
                        i0 = i0[:3]
                    dir_ = dir_ + str(i0)+'_'+str(i1)
                else:
                    pass
        self.dir_ = dir_
        # transverse map initial values
        self.__input__()
        self.n_segments = 1
#         self.Q_x = 278.225
        self.Qp_x=0 # dQ/(dp/p)
#         self.Q_y = 277.29
        self.Qp_y=0
        self.chroma = Chromaticity(Qp_x=[self.Qp_x], Qp_y=[self.Qp_y])
#         self.beta_x_inj = self.circumference/(2*np.pi*self.Q_x)
        self.D_x_inj = 0
#         self.beta_y_inj = self.circumference/(2*np.pi*self.Q_y)
        self.D_y_inj = 0 
        self.alpha_x_inj = 0
        self.alpha_y_inj = 0
        self._dir_()
        self.map_()
        self.SR_()
        self.generate_6D_Gaussian_bunch()
        self.slicer_()
        self.wake_()
        self.one_turn_map_()
        self.stat_()
        self.track_()
        self.dump_data()
    
    
    
    def _dir_(self):
#         parent_fig_directory = os.path.dirname(os.getcwd())
#         self.input_dir = parent_fig_directory + '/input_data'
        # parent_dir = str(Path(os.getcwd()).resolve().parents[0])
        parent_dir = os.path.dirname(os.path.dirname(os.getcwd()))
        self.input_dir = parent_dir + '/input_data'
        if self.dir_name :
            self.dir_ = self.dir_name + '/'
        else:
            self.dir_ = 'part' + str(int(self.n_particles/1e10)) + 'e10_freq' + str(int(self.frequency*1e-6)) + '_sz'+str(self.sigma_z) + '_mode_'+self.mode+'/'
        self.res_directory = parent_dir + '/results/' + self.dir_
        self.fig_directory = self.res_directory + 'figures/'
        self.data_directory = self.res_directory +'data/'
        if os.path.exists(self.res_directory)==False:
            os.makedirs(self.res_directory)
        if os.path.exists(self.fig_directory)==False:
            os.makedirs(self.fig_directory)
        if os.path.exists(self.data_directory)==False:
            os.makedirs(self.data_directory)
        if os.path.exists(self.data_directory + 'moments')==False:
            os.makedirs(self.data_directory + 'moments')
#         self.fig_directory = self.parent_fig_directory + str(self.n_particles/1e10)+'e10/'  
        delete_folder = False
        if os.path.exists(self.fig_directory) and delete_folder:    
            shutil.rmtree(self.fig_directory)
            os.makedirs(self.fig_directory)
        folders_names = ['', 'phasespaces_xxp', 'phasespaces_yyp', 'phasespaces_zdp',
                     'profile_x', 'profile_y', 'profile_z'
                     ]
        for i in folders_names:
            try:
                os.makedirs(self.fig_directory+i)
            except:
                pass 
        json.dump(self.d, open(self.res_directory+'input_conf.json', 'w'), indent=4)


    def __input__(self):
        if self.wake==True:
            self.use_transv_wakeY = True
            self.use_long_wake = True
            self.use_transv_wakeX = True
        else:
            self.use_transv_wakeY = False
            self.use_long_wake = False
            self.use_transv_wakeX = False
        M = beam_param(mode=self.mode, freq=self.frequency)
        self.energy = M.Etot
        if self.sigma_z is None:
            self.sigma_z = M.sigma_z
        self.alpha = M.alpha
        self.sigma_e = M.sigma_e
        self.eq_sig_dp = M.sig_e_eq
        # self.eq_sig_dp = 3e-4
        if self.dpt:    
            dpt = self.dpt
        else:
            dpt = M.dpt
            self.dpt = dpt
        if self.epsn_x == None:
            self.epsn_x = M.epsn_x
        if self.epsn_y == None:
            self.epsn_y = M.epsn_y
        self.h_RF = M.h
        self.E_loss = M.U0
        # self.emit_eq = M.emit_eq
        V0 = M.Vtot
        print('V0 ', V0*1e-6)
        # V0 = 60e+6
        if self.V_RF == None:
            self.V_RF = fsolve(dp_vrf, V0, (dpt, self.frequency, self.mode))[0]
        self.p0 = self.energy * cons.e /cons.c
        self.gamma = (1 / (cons.c*self.mass) *np.sqrt(self.p0**2+ self.mass**2*cons.c**2))
        self.beta = np.sqrt(1 - self.gamma ** -2)
        self.betagamma = np.sqrt(self.gamma ** 2 - 1)
        self.abs_eta = np.abs(self.alpha - 1/self.gamma**2)
        self.eq_emit_x = M.epsnx_eq # normalized equ emittance from class beam param
        self.eq_emit_y = M.epsny_eq # normalized equ emittance from class beam param
        # self.eq_emit_x = self.gamma*0.3e-9 # normalized emittance ~ 10um
        # self.eq_emit_y = self.gamma*1.42e-12 # 10um ou 1um -> voir plage de l'émittance acceptable
        self.Q_x = M.Q_x
        self.beta_x_inj = self.circumference/(2*np.pi*self.Q_x)
        self.Q_y = M.Q_y
        self.beta_y_inj = self.circumference/(2*np.pi*self.Q_y)
        
    def map_(self):
        # initialize arrays for segments (can also be different for each segment)
        self.s = np.arange(0, self.n_segments + 1) * self.circumference / self.n_segments
        alpha_x = self.alpha_x_inj * pm.ones(self.n_segments)
        beta_x = self.beta_x_inj * pm.ones(self.n_segments)
        D_x = self.D_x_inj * pm.ones(self.n_segments)
        alpha_y = self.alpha_y_inj * pm.ones(self.n_segments)
        beta_y = self.beta_y_inj * pm.ones(self.n_segments)
        D_y = self.D_y_inj * pm.ones(self.n_segments)
        # create transverse map
        self.transverse_map = TransverseMap(
            self.s, alpha_x, beta_x, D_x, alpha_y, beta_y, D_y, self.Q_x, self.Q_y, [self.chroma])
        #CREATE LONGITUDINAL MAP
        # longitudinal_map = LinearMap([alpha], circumference, Q_s)
        self.longitudinal_map = RFSystems(
            circumference=self.circumference,
            harmonic_list=[self.h_RF],  
            voltage_list=[self.V_RF],
            phi_offset_list= [self.dphi_RF],
            alpha_array=[self.alpha],
            gamma_reference=self.gamma,
            p_increment=self.p_increment,
            D_x=[0],
            D_y=[0],
            mass = self.mass,
            charge = self.charge 
          )
        
    def SR_(self):
        ## EVALUATE NEW STABLE PHASE DUE TO ENERGY LOST BY SR
        self.phi_s = pm.arcsin(self.E_loss/self.longitudinal_map.voltages[0])
        self.z_s = self.circumference*self.phi_s/(2*np.pi*self.longitudinal_map.harmonics[0])
        self.rfbucket=self.longitudinal_map.get_bucket(gamma=self.gamma, mass=self.mass, charge=self.charge)
        self.Q_s_corrected = self.rfbucket.Q_s*pm.sqrt(pm.cos(self.phi_s))
        ## SYNCHROTRON RADIATION LONGITUDINAL
        self.SynchrotronRadiationLong = SynchrotronRadiationLongitudinal(
            eq_sig_dp=self.eq_sig_dp, damping_time_z_turns=self.damping_time_z_turns, 
            E_loss_eV=self.E_loss)
        ## SYNCHROTRON RADIATION TRANSVERSE
        self.damping_time_x_turns = 2*self.damping_time_z_turns
        self.damping_time_y_turns = 2*self.damping_time_z_turns
        self.SynchrotronRadiationTrans = SynchrotronRadiationTransverse(
                eq_emit_x=self.eq_emit_x, eq_emit_y=self.eq_emit_y, 
            damping_time_x_turns=self.damping_time_x_turns, 
            damping_time_y_turns=self.damping_time_y_turns, 
            beta_x=self.transverse_map.beta_x[-1], 
            beta_y=self.transverse_map.beta_y[-1])
        ## INITIAL BEAM PARAMETERS
#         self.sigma_z = self.abs_eta * self.circumference * self.eq_sig_dp / (2*np.pi*self.Q_s_corrected) # a forcer pour voir l'evolution
#         self.epsn_x = #self.eq_emit_x
#         self.epsn_y = #self.eq_emit_y

    def generate_6D_Gaussian_bunch(self):    
        # generate particle bunch
        longitudinal_mode = self.longitudinal_mode
        if  longitudinal_mode == "linear":
            check_inside_bucket = lambda z, dp: pm.array(len(z) * [True])
        elif longitudinal_mode == "non-linear":
            check_inside_bucket = self.rfbucket.make_is_accepted(margin=0.05)
        else:
            raise NotImplementedError("Something wrong with longitudinal_mode")

        epsx_geo = self.epsn_x / self.betagamma
        epsy_geo = self.epsn_y / self.betagamma

        injection_optics = self.transverse_map.get_injection_optics()
        self.bunch = generators.ParticleGenerator(
            macroparticlenumber=self.n_macroparticles,
            intensity=self.n_particles,
            charge=self.charge,
            mass=self.mass,
            circumference=self.circumference,
            gamma=self.gamma,
            distribution_x=generators.gaussian2D(epsx_geo),
            alpha_x=injection_optics["alpha_x"],
            beta_x=injection_optics["beta_x"],
            D_x=injection_optics["D_x"],
            distribution_y=generators.gaussian2D(epsy_geo),
            alpha_y=injection_optics["alpha_y"],
            beta_y=injection_optics["beta_y"],
            D_y=injection_optics["D_y"],
            distribution_z=generators.cut_distribution(
                generators.gaussian2D_asymmetrical(sigma_u=self.sigma_z, 
                                                   sigma_up=self.sigma_dp),
                is_accepted=check_inside_bucket,
            ),
        ).generate()
        self.rfbucket = self.longitudinal_map.get_bucket(self.bunch)
        self.rfbucket_wosc = copy.deepcopy(self.rfbucket)
        self.bunch.z += self.z_s
        # align bunch longitudinally around centre of bucket
        # self.bunch.z += self.rfbucket.z_sfp_extr

    def slicer_(self):
        ## CREATE BEAM SLICER
        bunch = self.bunch
        if self.slicing_mode == 'from_first_to_last_particles':
            self.slicer = UniformBinSlicer(self.n_slices)
            self.initial_cut_tail_z = pm.min(bunch.z) 
            self.initial_cut_head_z = pm.max(bunch.z)
        elif self.slicing_mode == 'fixed_cuts':
            self.initial_cut_tail_z = pm.min(bunch.z) - self.fixed_cuts_perc_min_max*(pm.max(bunch.z)-pm.min(bunch.z))
            self.initial_cut_head_z = pm.max(bunch.z) + self.fixed_cuts_perc_min_max*(pm.max(bunch.z)-pm.min(bunch.z))
            self.slicer = UniformBinSlicer(self.n_slices, z_cuts=(self.initial_cut_tail_z,self.initial_cut_head_z))

    def wake_(self):
        ## CREATE WAKE
        use_long_wake = self.use_long_wake
        use_transv_wakeX = self.use_transv_wakeX
        use_transv_wakeY = self.use_transv_wakeY
        # CREATE LONGITUDINAL WAKE
        if use_long_wake:
            list_of_Lwake_sources = [] 
            if self.copper:
                L_wake_file = self.input_dir + '/wake_long_copper_PyHT.txt'
            else:
                L_wake_file = self.input_dir + '/wake_long_stainless_PyHT.txt'
            L_wakefile_columns = ['time', 'longitudinal']
            L_wake_table = WakeTable(L_wake_file, L_wakefile_columns)
            list_of_Lwake_sources.append(L_wake_table)
        # CREATE TRANSVERSE WAKE
        if use_transv_wakeX or use_transv_wakeY:
            list_of_Twake_sources = []    
        # # CREATE TRANSVERSE WAKE X
        if use_transv_wakeX:
            if self.copper:
                T_wake_x_file = self.input_dir + '/wake_tr_copper_PyHT.txt'
            else:
                T_wake_x_file = self.input_dir + '/wake_tr_stainless_PyHT.txt'
            T_wake_x = np.loadtxt(T_wake_x_file, dtype=float)
            T_wakefile_columns_x = ['time', 'dipole_x']
            temp_file = 'temp.txt'
            np.savetxt(temp_file, np.transpose([T_wake_x[:,0],T_wake_x[:,1]/self.n_segments]))
            T_wake_table_x = WakeTable(temp_file, T_wakefile_columns_x)
            T_wake_table_x = WakeTable(temp_file, T_wakefile_columns_x)
            os.remove(temp_file)
            list_of_Twake_sources.append(T_wake_table_x)
        # # CREATE TRANSVERSE WAKE Y
        if use_transv_wakeY:
            T_wake_y_file = self.input_dir + '/wake_tr_copper_PyHT.txt'
            T_wake_y = np.loadtxt(T_wake_y_file, dtype=float)
            T_wakefile_columns_y = ['time', 'dipole_y']
            temp_file_y = 'temp.txt'
            np.savetxt(temp_file, np.transpose([T_wake_y[:,0],T_wake_y[:,1]/self.n_segments]))
            T_wake_table_y = WakeTable(temp_file, T_wakefile_columns_y)
            os.remove(temp_file)
            list_of_Twake_sources.append(T_wake_table_y)
        # TOTAL WAKE
        if use_long_wake:
            self.Lwake_fields = WakeField(self.slicer, *list_of_Lwake_sources)
            self.plot_wake = self.Lwake_fields
        else:
            self.Lwake_fields = False  
        if use_transv_wakeX or use_transv_wakeY:
            self.Twake_fields = WakeField(self.slicer, *list_of_Twake_sources)    
            self.plot_wake = self.Twake_fields
        else:
            self.Twake_fields = False            
    # ONE TURN MAP
    def one_turn_map_(self):
        self.one_turn_map = []
        for tr_ele, ele_len in zip(self.transverse_map, np.diff(self.s)):
            self.one_turn_map.append(tr_ele)
            if self.Twake_fields:
                self.one_turn_map.append(self.Twake_fields)
        self.one_turn_map.append(self.longitudinal_map)
        self.one_turn_map.append(self.SynchrotronRadiationLong)
        self.one_turn_map.append(self.SynchrotronRadiationTrans)
        if self.Lwake_fields:
            self.one_turn_map.append(self.Lwake_fields)

    def dump_data(self):
        ## DUMP DATA FUNCTION
        # mean_x_array = self.mean_x_array
        # mean_y_array = self.mean_y_array
        # mean_z_array = self.mean_z_array
        # mean_xp_array = self.mean_xp_array
        # mean_yp_array = self.mean_yp_array
        # mean_dp_array = self.mean_dp_array
        mom_x = self.mom_x
        # mom_y = self.mom_y
        self.mom_z = self.mom_z
        # sigma_x_array = self.sigma_x_array
        # sigma_y_array = self.sigma_y_array
        # sigma_z_array = self.sigma_z_array
        # sigma_xp_array = self.sigma_xp_array
        # sigma_yp_array = self.sigma_yp_array
        # sigma_dp_array = self.sigma_dp_array
        bunch = self.bunch
        if self.wake:
            plot_wake = self.plot_wake
        ## Save bunch coordinates to H5 file
        if self.save_bunch_at_end:
            hf_bunch = h5py.File(self.data_directory+'bunch_coordinates.h5', 'w')
            dset = hf_bunch.create_dataset('x', data=bunch.x)
            dset.attrs['Units'] = '[m]'
            dset = hf_bunch.create_dataset('y', data=bunch.y)
            dset.attrs['Units'] = '[m]'
            dset = hf_bunch.create_dataset('z', data=bunch.z)
            dset.attrs['Units'] = '[m]'
            dset = hf_bunch.create_dataset('xp', data=bunch.xp)
            dset.attrs['Units'] = '[rad]'
            dset = hf_bunch.create_dataset('yp', data=bunch.yp)
            dset.attrs['Units'] = '[rad]'
            dset = hf_bunch.create_dataset('dp', data=bunch.dp)
            dset.attrs['Units'] = '[1]'
            hf_bunch.close()
        ## Save transverse moments to H5 file
        ## X plane
        hf_x_moments = h5py.File(self.data_directory+'/moments/x_moments'+str(self.n_particles/1e10)+'.h5', 'w')
        dset = hf_x_moments.create_dataset('x_moments', data=mom_x)
        dset.attrs['Units'] = '<x> [m], <x*(z-<z>)> [m**2], ...'
        dset = hf_x_moments.create_dataset('n_particles', data=self.n_particles)
        dset.attrs['Units'] = '[1]'
        dset = hf_x_moments.create_dataset('Q_s', data=self.Q_s_corrected)
        dset.attrs['Units'] = '[1]'
        dset = hf_x_moments.create_dataset('Q_x', data=self.Q_x)
        dset.attrs['Units'] = '[1]'
        hf_x_moments.close()
        ## Save transverse moments to H5 file
        ## Y plane
        hf_y_moments = h5py.File(self.data_directory+'/moments/y_moments'+str(self.n_particles/1e10)+'.h5', 'w')
        dset = hf_y_moments.create_dataset('y_moments', data=self.mom_y)
        dset.attrs['Units'] = '<y> [m], <y*(z-<z>)> [m**2], ...'
        dset = hf_y_moments.create_dataset('n_particles', data=self.n_particles)
        dset.attrs['Units'] = '[1]'
        dset = hf_y_moments.create_dataset('Q_s', data=self.Q_s_corrected)
        dset.attrs['Units'] = '[1]'
        dset = hf_y_moments.create_dataset('Q_y', data=self.Q_y)
        dset.attrs['Units'] = '[1]'
        hf_y_moments.close()
        ## Save longitudinal moments to H5 file
        hf_z_moments = h5py.File(self.data_directory+'/moments/z_moments'+str(self.n_particles/1e10)+'.h5', 'w')
        dset = hf_z_moments.create_dataset('z_moments', data=self.mom_z)
        dset.attrs['Units'] = '<z> [m], <(z-<z>)**2> [m**2], ...'
        dset = hf_z_moments.create_dataset('n_particles', data=self.n_particles)
        dset.attrs['Units'] = '[1]'
        dset = hf_z_moments.create_dataset('Q_s', data=self.Q_s_corrected)
        dset.attrs['Units'] = '[1]'
        hf_z_moments.close()
        ## Save statistics and parameters to H5 file
        hf_ss = h5py.File(self.data_directory+'statistics_and_parameters.h5', 'w')
        dset = hf_ss.create_dataset('mean_x_array', data=self.mean_x_array*1e6)
        dset.attrs['Units'] = '[um]'
        dset = hf_ss.create_dataset('mean_y_array', data=self.mean_y_array*1e6)
        dset.attrs['Units'] = '[um]'
        dset = hf_ss.create_dataset('mean_z_array', data=self.mean_z_array*1e3)
        dset.attrs['Units'] = '[mm]'
        dset = hf_ss.create_dataset('mean_xp_array', data=self.mean_xp_array*1e9)
        dset.attrs['Units'] = '[nrad]'
        dset = hf_ss.create_dataset('mean_yp_array', data=self.mean_yp_array*1e9)
        dset.attrs['Units'] = '[nrad]'
        dset = hf_ss.create_dataset('mean_dp_array', data=self.mean_dp_array*1e2)
        dset.attrs['Units'] = '[%]'
        dset = hf_ss.create_dataset('sigma_x_array', data=self.sigma_x_array*1e6)
        dset.attrs['Units'] = '[um]'
        dset = hf_ss.create_dataset('sigma_y_array', data=self.sigma_y_array*1e6)
        dset.attrs['Units'] = '[um]'
        dset = hf_ss.create_dataset('sigma_z_array', data=self.sigma_z_array*1e3)
        dset.attrs['Units'] = '[mm]'
        dset = hf_ss.create_dataset('sigma_xp_array', data=self.sigma_xp_array*1e9)
        dset.attrs['Units'] = '[nrad]'
        dset = hf_ss.create_dataset('sigma_yp_array', data=self.sigma_yp_array*1e9)
        dset.attrs['Units'] = '[nrad]'
        dset = hf_ss.create_dataset('sigma_dp_array', data=self.sigma_dp_array*1e2)
        dset.attrs['Units'] = '[%]'
        dset = hf_ss.create_dataset('epsn_x_array', data=self.epsn_x_array*1e2)
        dset.attrs['Units'] = '[pm rad]'
        dset = hf_ss.create_dataset('epsn_y_array', data=self.epsn_y_array*1e2)
        dset.attrs['Units'] = '[pm rad]'
        dset = hf_ss.create_dataset('alpha', data=self.alpha)
        dset.attrs['Units'] = '[1]'
        dset = hf_ss.create_dataset('energy', data=self.energy*1e-9)
        dset.attrs['Units'] = '[GeV]'
        dset = hf_ss.create_dataset('Q_x', data=self.Q_x)
        dset.attrs['Units'] = '[1]'
        dset = hf_ss.create_dataset('Q_y', data=self.Q_y)
        dset.attrs['Units'] = '[1]'
        dset = hf_ss.create_dataset('Q_s', data=self.Q_s_corrected)
        dset.attrs['Units'] = '[1]'
        dset = hf_ss.create_dataset('gamma rel', data=self.gamma)
        dset.attrs['Units'] = '[1]'
        dset = hf_ss.create_dataset('V_RF', data=self.V_RF*1e-6)
        dset.attrs['Units'] = '[MeV]'
        dset = hf_ss.create_dataset('h_RF', data=self.h_RF)
        dset.attrs['Units'] = '[]'
        dset = hf_ss.create_dataset('n_turns', data=self.n_turns)
        dset.attrs['Units'] = '[1]'
        dset = hf_ss.create_dataset('n_particles', data=self.n_particles/1e10)
        dset.attrs['Units'] = '[1e10]'
        dset = hf_ss.create_dataset('n_macroparticles', data=self.n_macroparticles)
        dset.attrs['Units'] = '[1]'
        dset = hf_ss.create_dataset('n_slices', data=self.n_slices)
        dset.attrs['Units'] = '[1]'
        dset = hf_ss.create_dataset('initial z-bin length', data=
                                    (self.initial_cut_head_z-self.initial_cut_tail_z)*1e3/self.n_slices)
        dset.attrs['Units'] = '[mm]'
        if self.wake:
            dset = hf_ss.create_dataset('final z-bin length', data=
                                    (plot_wake.slice_set_deque[0].z_cut_head-
                                     plot_wake.slice_set_deque[0].z_cut_tail)*1e3/self.n_slices)
        dset.attrs['Units'] = '[mm]'
        dset = hf_ss.create_dataset('slicing_mode', data=self.slicing_mode)
        dset.attrs['Units'] = 'string'
        dset = hf_ss.create_dataset('E_loss', data=self.E_loss*1e-6)
        dset.attrs['Units'] = '[MeV]'
        dset = hf_ss.create_dataset('epsn_x', data=self.epsn_x*1e12)
        dset.attrs['Units'] = '[pm rad]'
        dset = hf_ss.create_dataset('epsn_y', data=self.epsn_y*1e12)
        dset.attrs['Units'] = '[pm rad]'
        dset = hf_ss.create_dataset('sigma_z', data=self.sigma_z*1e3)
        dset.attrs['Units'] = '[mm]'
        dset = hf_ss.create_dataset('damping_time_x_turns', data=self.damping_time_x_turns)
        dset.attrs['Units'] = '[# turns]'
        dset = hf_ss.create_dataset('damping_time_y_turns', data=self.damping_time_y_turns)
        dset.attrs['Units'] = '[# turns]'
        dset = hf_ss.create_dataset('damping_time_z_turns', data=self.damping_time_z_turns)
        dset.attrs['Units'] = '[# turns]'
        dset = hf_ss.create_dataset('eq_emit_x', data=self.eq_emit_x*1e12)
        dset.attrs['Units'] = '[pm rad]'
        dset = hf_ss.create_dataset('eq_emit_y', data=self.eq_emit_y*1e12)
        dset.attrs['Units'] = '[pm rad]'
        dset = hf_ss.create_dataset('eq_sig_dp', data=self.eq_sig_dp*1e2)
        dset.attrs['Units'] = '[%]'
        dset = hf_ss.create_dataset('z_bin_centers', data=self.z_bin_centers)
        dset.attrs['Units'] = 'z [m]'
        dset = hf_ss.create_dataset('x_of_z', data=self.x_of_z)
        dset.attrs['Units'] = 'x(z) [m]'
        dset = hf_ss.create_dataset('Nx_of_z', data=self.Nx_of_z)
        dset.attrs['Units'] = 'N(z) [1]'
        dset = hf_ss.create_dataset('index_x_Turn_record', data=self.index_x_Turn_record)
        dset.attrs['Units'] = 'N(z) [1]'
        dset = hf_ss.create_dataset('y_of_z', data=self.y_of_z)
        dset.attrs['Units'] = 'y(z) [m]'
        dset = hf_ss.create_dataset('Ny_of_z', data=self.Ny_of_z)
        dset.attrs['Units'] = 'N(z) [1]'
        dset = hf_ss.create_dataset('index_y_Turn_record', data=self.index_y_Turn_record)
        dset.attrs['Units'] = 'N(z) [1]'
        hf_ss.close()

    def stat_(self):
        ## STATISTICS AT TURN 0
        # Initialization
        n_turns = self.n_turns
        max_x_mom_order = self.max_x_mom_order
        max_y_mom_order = self.max_y_mom_order
        max_z_mom_order = self.max_z_mom_order
        n_slices = self.n_slices
        bunch = self.bunch
        damp_slice_n_turns = self.damp_slice_n_turns
        #
        mean_x_array = pm.zeros(n_turns+1)
        mean_y_array = pm.zeros(n_turns+1)
        mean_z_array = pm.zeros(n_turns+1)
        sigma_x_array = pm.zeros(n_turns+1)
        sigma_y_array = pm.zeros(n_turns+1)
        sigma_z_array = pm.zeros(n_turns+1)
        mean_xp_array = pm.zeros(n_turns+1)
        mean_yp_array = pm.zeros(n_turns+1)
        mean_dp_array = pm.zeros(n_turns+1)
        sigma_xp_array = pm.zeros(n_turns+1)
        sigma_yp_array = pm.zeros(n_turns+1)
        sigma_dp_array = pm.zeros(n_turns+1)
        epsn_x_array = pm.zeros(n_turns+1)
        epsn_y_array = pm.zeros(n_turns+1)
        mom_x=pm.zeros((max_x_mom_order+1, n_turns+1))
        mom_y=pm.zeros((max_y_mom_order+1, n_turns+1))
        mom_z=pm.zeros((max_z_mom_order+1, n_turns+1))
        if self.slicing_mode == 'fixed_cuts':
            self.z_bin_centers = pm.zeros((n_slices, 1))
        elif self.slicing_mode == 'from_first_to_last_particles':
            self.z_bin_centers = pm.zeros((n_slices, damp_slice_n_turns))
        self.x_of_z = pm.zeros((n_slices, damp_slice_n_turns))
        self.y_of_z = pm.zeros((n_slices, damp_slice_n_turns))
        self.Nx_of_z = pm.zeros((n_slices, damp_slice_n_turns))
        self.Ny_of_z = pm.zeros((n_slices, damp_slice_n_turns))
        # Setting zero element
        mean_x_array[0] = bunch.mean_x()
        mean_y_array[0] = bunch.mean_y()
        mean_z_array[0] = bunch.mean_z()
        sigma_x_array[0] = bunch.sigma_x()
        sigma_y_array[0] = bunch.sigma_y()
        sigma_z_array[0] = bunch.sigma_z()
        mean_xp_array[0] = bunch.mean_xp()
        mean_yp_array[0] = bunch.mean_yp()
        mean_dp_array[0] = bunch.mean_dp()
        sigma_xp_array[0] = bunch.sigma_xp()
        sigma_yp_array[0] = bunch.sigma_yp()
        sigma_dp_array[0] = bunch.sigma_dp()
        epsn_x_array[0] = bunch.effective_normalized_emittance_x()
        epsn_y_array[0] = bunch.effective_normalized_emittance_y()
        mom_x[0,0] = mean_x_array[0]
        mom_y[0,0] = mean_y_array[0]
        mom_z[0,0] = mean_z_array[0]
        self.z_centred = bunch.z-mean_z_array[0]
        self.z_centred_powered = self.z_centred
        for ii in range(1, self.max_x_mom_order+1):
            mom_x[ii,0] = pm.mean(bunch.x*self.z_centred_powered)
            self.z_centred_powered = self.z_centred_powered*self.z_centred

        for ii in range(1, self.max_y_mom_order+1):
            mom_y[ii,0] = pm.mean(bunch.y*self.z_centred_powered)
            self.z_centred_powered = self.z_centred_powered*self.z_centred

        self.z_centred_powered = self.z_centred
        for ii in range(1, self.max_z_mom_order+1):
            self.z_centred_powered = self.z_centred_powered*self.z_centred
            mom_z[ii,0] = np.mean(self.z_centred_powered)
        self.damp_slice_flag_x = False
        self.damp_slice_flag_y = False
        self.mean_x_array = mean_x_array
        self.mean_y_array = mean_y_array
        self.mean_z_array = mean_z_array
        self.mean_xp_array = mean_xp_array
        self.mean_yp_array = mean_yp_array
        self.mean_dp_array = mean_dp_array
        self.mom_x = mom_x
        self.mom_y = mom_y
        self.mom_z = mom_z
        self.sigma_x_array = sigma_x_array
        self.sigma_y_array = sigma_y_array
        self.sigma_z_array = sigma_z_array
        self.sigma_xp_array = sigma_xp_array
        self.sigma_yp_array = sigma_yp_array
        self.sigma_dp_array = sigma_dp_array
        self.epsn_x_array = epsn_x_array
        self.epsn_y_array = epsn_y_array

    def track_(self):
        ## TRACKING
        # RF bucket
        num = 2000
        bunch = self.bunch
        n_turns = self.n_turns
        rfbucket_wosc = self.rfbucket_wosc
        rfbucket = self.rfbucket
        hc = rfbucket.separatrix
        # hc_wosc = rfbucket_wosc.separatrix
        # #
        one_turn_map = self.one_turn_map
        mom_z = self.mom_z
        if self.wake:
            plot_wake = self.plot_wake
        pbar = tqdm(range(1, n_turns+1))
        # for i_turn in range(1, n_turns+1):
        for i_turn in pbar:
            if __name__ == "__main__": 
                # print('Turn %d/%d'%(i_turn, n_turns))
                pbar.set_description('Turn %d/%d'%(i_turn, n_turns))
            for m in one_turn_map:
                m.track(bunch)
            # Statistics computed each turn
            self.mean_x_array[i_turn] = bunch.mean_x()
            self.sigma_x_array[i_turn] = bunch.sigma_x()
            self.mean_z_array[i_turn] = bunch.mean_z()
            self.sigma_z_array[i_turn] = bunch.sigma_z()
            self.sigma_dp_array[i_turn] = bunch.sigma_dp()              
            self.mean_y_array[i_turn] = bunch.mean_y()  
            self.sigma_y_array[i_turn] = bunch.sigma_y()
            self.epsn_x_array[i_turn] = bunch.effective_normalized_emittance_x()
            self.epsn_y_array[i_turn] = bunch.effective_normalized_emittance_y()
    #        mean_xp_array[i_turn] = bunch.mean_xp()
    #        mean_yp_array[i_turn] = bunch.mean_yp()
    #        mean_dp_array[i_turn] = bunch.mean_dp()
    #        sigma_xp_array[i_turn] = bunch.sigma_xp()
    #        sigma_yp_array[i_turn] = bunch.sigma_yp()
            # Compute x moments
            if self.compute_x_moments:
                self.mom_x[0,i_turn] = self.mean_x_array[i_turn]
                self.z_centred = bunch.z-self.mean_z_array[i_turn]
                self.z_centred_powered = self.z_centred
                for ii in range(1, self.max_x_mom_order+1):
                    self.mom_x[ii,i_turn] = pm.mean(bunch.x*self.z_centred_powered)
                    self.z_centred_powered = self.z_centred_powered*self.z_centred
            # Compute y moments
            if self.compute_y_moments:
                self.mom_y[0,i_turn] = self.mean_y_array[i_turn]
                self.z_centred = bunch.z-self.mean_z_array[i_turn]
                self.z_centred_powered = self.z_centred
                for ii in range(1, self.max_y_mom_order+1):
                    self.mom_y[ii,i_turn] = pm.mean(bunch.y*self.z_centred_powered)
                    self.z_centred_powered = self.z_centred_powered*self.z_centred
            # Compute z moments
            if self.compute_z_moments:
                self.mom_z[0,i_turn] = self.mean_z_array[i_turn]
                self.z_centred = bunch.z-mean_self.z_array[i_turn]
                self.z_centred_powered = self.z_centred
                for ii in range(1, self.max_z_mom_order+1):
                    self.z_centred_powered = self.z_centred_powered*self.z_centred
                    self.mom_z[ii,i_turn] = pm.mean(self.z_centred_powered)
            # Save x(z) only for n_turns_damp_slice turns starting when mean_x > 10*std_x_0
            if self.damp_slice_flag_x == False and (np.abs(self.mean_x_array[i_turn]) > 10*self.sigma_x_array[0] or 
                                             i_turn > n_turns-self.damp_slice_n_turns):
                self.damp_slice_flag_x = True
                damp_slice_counter_x = 1
                self.index_x_Turn_record = i_turn
            if self.damp_slice_flag_x == True and damp_slice_counter_x <= self.damp_slice_n_turns and self.wake:
                if self.slicing_mode == 'fixed_cuts':
                    self.z_bin_centers[:,0] = \
                         plot_wake.slice_set_deque[0].z_centers
                elif self.slicing_mode == 'from_first_to_last_particles':
                    self.z_bin_centers[:,damp_slice_counter_x-1] = \
                         plot_wake.slice_set_deque[0].z_centers
                self.x_of_z[:,damp_slice_counter_x-1] =  \
                    plot_wake.slice_set_deque[0].mean_x
                self.Nx_of_z[:,damp_slice_counter_x-1] =  \
                    plot_wake.slice_set_deque[0].n_macroparticles_per_slice
                damp_slice_counter_x = damp_slice_counter_x +1
            # Save y(z) only for n_turns_damp_slice turns starting when mean_y > 10*std_y_0
            if self.damp_slice_flag_y == False and (np.abs(self.mean_y_array[i_turn]) > 10*self.sigma_y_array[0] or 
                                             i_turn > n_turns-self.damp_slice_n_turns):
                self.damp_slice_flag_y = True
                damp_slice_counter_y = 1
                self.index_y_Turn_record = i_turn
            if self.damp_slice_flag_y == True and damp_slice_counter_y <= self.damp_slice_n_turns and self.wake:
                if self.slicing_mode == 'fixed_cuts':
                    self.z_bin_centers[:,0] = \
                         plot_wake.slice_set_deque[0].z_centers
                elif self.slicing_mode == 'from_first_to_last_particles':
                    self.z_bin_centers[:,damp_slice_counter_y-1] = \
                         plot_wake.slice_set_deque[0].z_centers
                self.y_of_z[:,damp_slice_counter_y-1] =  \
                    plot_wake.slice_set_deque[0].mean_y
                self.Ny_of_z[:,damp_slice_counter_y-1] =  \
                    plot_wake.slice_set_deque[0].n_macroparticles_per_slice
                damp_slice_counter_y = damp_slice_counter_y +1
            if i_turn%self.plot_every==0 or i_turn==n_turns or i_turn==1 or \
                            (i_turn <= self.plot_every and i_turn%self.plot_every_finer==0):
                # try:
                #     bunch_profile = h5py.File(self.data_directory+'bunch_profile'+str(i_turn)+'.h5', 'w')
                # except:
                #     bunch_profile.close()
                #     bunch_profile = h5py.File(self.data_directory+'bunch_profile'+str(i_turn)+'.h5', 'w')
#                 try:
                # evolutions
                plt.clf()
                plt.plot(self.mean_x_array[:i_turn+1]*1e6, linewidth=0.5)
                plt.xlabel('time [# turns]')
                plt.ylabel('mean x [um]')
                plt.savefig(self.fig_directory+'mean_x.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('mean_x', data=self.mean_x_array[:i_turn+1], compression="gzip", compression_opts=9) 
                # dset.attrs['Units'] = '[m]'

                plt.clf()
                ax = plt.gca()
                ax.plot(self.mean_y_array[:i_turn+1]*1e6, linewidth=0.5)
                plt.xlabel('time [# turns]')
                ax.set_ylabel('mean y [um]')
                plt.savefig(self.fig_directory+'mean_y.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('mean_y', data=self.mean_y_array[:i_turn+1], compression="gzip", compression_opts=9) 

                plt.clf()
                ax = plt.gca()
                ax.plot(self.mean_z_array[:i_turn+1]*1e3, linewidth=0.5)
                plt.xlabel('time [# turns]')
                ax.set_ylabel('mean z [mm]')
                plt.savefig(self.fig_directory+'mean_z.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('mean_z', data=self.mean_z_array[:i_turn+1], compression="gzip", compression_opts=9) 

                plt.clf()
                ax = plt.gca()
                ax.plot(self.mean_xp_array[:i_turn+1]*1e9, linewidth=0.5)
                plt.xlabel('time [# turns]')
                ax.set_ylabel('mean xp [nrad]')
                plt.savefig(self.fig_directory+'mean_xp.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('mean_xp', data=self.mean_xp_array[:i_turn+1], compression="gzip", compression_opts=9) 

                plt.clf()
                ax = plt.gca()
                ax.plot(self.mean_yp_array[:i_turn+1]*1e9, linewidth=0.5)
                plt.xlabel('time [# turns]')
                ax.set_ylabel('mean yp [nrad]')
                plt.savefig(self.fig_directory+'mean_yp.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('mean_yp', data=self.mean_yp_array[:i_turn+1], compression="gzip", compression_opts=9) 

                plt.clf()
                ax = plt.gca()
                ax.plot(self.mean_dp_array[:i_turn+1]*1e2, linewidth=0.5)
                plt.xlabel('time [# turns]')
                ax.set_ylabel('mean dp [%]')
                plt.savefig(self.fig_directory+'mean_dp.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('mean_dp', data=self.mean_dp_array[:i_turn+1], compression="gzip", compression_opts=9) 

                plt.clf()
                ax = plt.gca()
                ax.plot(self.sigma_x_array[:i_turn+1]*1e6, linewidth=0.5)
                plt.xlabel('time [# turns]')
                ax.set_ylabel('sigma x [um]')
                plt.savefig(self.fig_directory+'sigma_x.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('sigma_x', data=self.sigma_x_array[:i_turn+1], compression="gzip", compression_opts=9) 

                plt.clf()
                ax = plt.gca()
                ax.plot(self.sigma_y_array[:i_turn+1]*1e6, linewidth=0.5)
                plt.xlabel('time [# turns]')
                ax.set_ylabel('sigma y [um]')
                plt.savefig(self.fig_directory+'sigma_y.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('sigma_y', data=self.sigma_y_array[:i_turn+1], compression="gzip", compression_opts=9) 

                plt.clf()
                ax = plt.gca()
                ax.plot(self.sigma_z_array[:i_turn+1]*1e3, linewidth=0.5)
                plt.xlabel('time [# turns]')
                ax.set_ylabel('sigma z [mm]')
                plt.savefig(self.fig_directory+'sigma_z.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('sigma_z', data=self.sigma_z_array[:i_turn+1], compression="gzip", compression_opts=9) 

                plt.clf()
                ax = plt.gca()
                ax.plot(self.sigma_xp_array[:i_turn+1]*1e9, linewidth=0.5)
                plt.xlabel('time [# turns]')
                ax.set_ylabel('sigma xp [nrad]')
                plt.savefig(self.fig_directory+'sigma_xp.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('sigma_xp', data=self.sigma_xp_array[:i_turn+1], compression="gzip", compression_opts=9) 

                plt.clf()
                ax = plt.gca()
                ax.plot(self.sigma_yp_array[:i_turn+1]*1e9, linewidth=0.5)
                plt.xlabel('time [# turns]')
                ax.set_ylabel('sigma yp [nrad]')
                plt.savefig(self.fig_directory+'sigma_yp.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('sigma_yp', data=self.sigma_yp_array[:i_turn+1], compression="gzip", compression_opts=9) 

                plt.clf()
                ax = plt.gca()
                ax.plot(self.sigma_dp_array[:i_turn+1]*1e2, '-', linewidth=0.5)
                plt.xlabel('time [# turns]')
                ax.set_ylabel('sigma dp [%]')
                plt.savefig(self.fig_directory+'sigma_dp.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('sigma_dp', data=self.sigma_dp_array[:i_turn+1], compression="gzip", compression_opts=9) 
                
                plt.clf()
                ax = plt.gca()
                ax.plot(self.epsn_x_array[:i_turn+1], '-', linewidth=0.5)
                plt.xlabel('time [# turns]')
                ax.set_ylabel('Effective normalized emittance x')
                plt.savefig(self.fig_directory+'epsn_x.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('epsn_x', data=self.epsn_x_array[:i_turn+1], compression="gzip", compression_opts=9) 

                plt.clf()
                ax = plt.gca()
                ax.plot(self.epsn_y_array[:i_turn+1], '-', linewidth=0.5)
                plt.xlabel('time [# turns]')
                ax.set_ylabel('Effective normalized emittance y')
                plt.savefig(self.fig_directory+'epsn_y.png', 
                    bbox_inches = 'tight')
                # dset = bunch_profile.create_dataset('epsn_y', data=self.epsn_y_array[:i_turn+1], compression="gzip", compression_opts=9) 
                
                # phasespaces zdp, xxp, yyp
                plt.clf()
                ax = plt.gca()
                zz = np.linspace(*rfbucket.interval, num=num)
                zz_bucket = zz[rfbucket.is_in_separatrix(zz, 0, margin=-0.001)]
                ax.plot(zz_bucket, +hc(zz_bucket), c='purple', lw=2, alpha=0.5, linestyle='--')
                ax.plot(zz_bucket, -hc(zz_bucket), c='purple', lw=2, alpha=0.5, linestyle='--')
                ppmax = np.maximum(rfbucket.dp_max(rfbucket.z_ufp_separatrix), 
                rfbucket_wosc.dp_max(rfbucket.z_ufp_separatrix))
                pp = np.linspace(-1.1*ppmax, 1.1*ppmax, num)
#                 zz = np.linspace(*rfbucket.interval, num=num)*1e-3
                ZZ, PP = np.meshgrid(zz, pp)
                orig_shape = ZZ.shape
                hh = rfbucket.hamiltonian(ZZ, PP, make_convex=True)
                ZZ = ZZ.reshape(orig_shape)
                PP = PP.reshape(orig_shape)
                hh = hh.reshape(orig_shape)
                ax.contour(ZZ, PP, hh, 40, alpha=0.5)
                nbins = 500
#                 H, xedges, yedges = np.histogram2d(bunch.z*1e3, bunch.dp*1e2, bins=nbins)
                H, xedges, yedges = np.histogram2d(bunch.z, bunch.dp, bins=nbins)
                H = np.rot90(H)
                H = np.flipud(H)
                Hmasked = np.ma.masked_where(H==0,H) 
                im = ax.pcolormesh(xedges,yedges,Hmasked)
                cbar = plt.colorbar(im)  
                ax.set_xlabel('z [m]')
                ax.set_ylabel('$\delta_p$')
                dpmax = 0.1e-2
                ax.axhline(dpmax, linestyle='--', color='r', linewidth=1, alpha=0.5)
                ax.axhline(-dpmax, linestyle='--', color='r', linewidth=1, alpha=0.5)
                if self.wake:
                    ax.axvline(plot_wake.slice_set_deque[0].z_cut_tail, linestyle='--', color='r', linewidth=1, alpha=0.5)
                    ax.axvline(plot_wake.slice_set_deque[0].z_cut_head, linestyle='--', color='r', linewidth=1, alpha=0.5)
#                 plt.xlim(np.min(bunch.z)*1e3, np.max(bunch.z)*1e3)
                ax.annotate("$\delta_p$ = 0.1 %", (-0.15, dpmax*1.2), xytext=None, xycoords='data', textcoords=None, color='red')
                ax.set_title('Turn '+str(i_turn))
                # plt.show()
                plt.savefig(self.fig_directory+'phasespaces_zdp/phase_space_'+str(i_turn)+'.png', 
                    bbox_inches = 'tight')
                # grp=bunch_profile.create_group('zdp')
                # grp.create_dataset('xedges',data=xedges, compression="gzip", compression_opts=9)
                # grp.create_dataset('yedges',data=yedges, compression="gzip", compression_opts=9)
                # grp.create_dataset('H',data=H, compression="gzip", compression_opts=9)
#                 dset = bunch_profile.create_dataset('ph_zdp', data=np.stack(xedges,yedges,Hmasked) )
                plt.clf()
                nbins = 500
                filter_not_nan = np.where((~np.isnan(bunch.x)) & (~np.isnan(bunch.xp)))[0]
                if len(filter_not_nan) > 0:
                    H, xedges, yedges = np.histogram2d(bunch.x[filter_not_nan]*1e6, bunch.xp[filter_not_nan]*1e9, bins=nbins)
                    H = np.rot90(H)
                    H = np.flipud(H)
                    Hmasked = np.ma.masked_where(H==0,H) 
                    plt.pcolormesh(xedges,yedges,Hmasked)
                    cbar = plt.colorbar()  
                    plt.xlabel('x [um]')
                    plt.ylabel('xp [nrad]')
                    plt.title('Turn '+str(i_turn))
                    plt.savefig(self.fig_directory+'phasespaces_xxp/phase_space_'+str(i_turn)+'.png', 
                        bbox_inches = 'tight')
                # grp=bunch_profile.create_group('xxp')
                # grp.create_dataset('xedges',data=xedges, compression="gzip", compression_opts=9)
                # grp.create_dataset('yedges',data=yedges, compression="gzip", compression_opts=9)
                # grp.create_dataset('H',data=H, compression="gzip", compression_opts=9)

                plt.clf()
                nbins = 500
                filter_not_nany = np.where((~np.isnan(bunch.y)) & (~np.isnan(bunch.yp)))[0]
                if len(filter_not_nany) > 0:
                    H, xedges, yedges = np.histogram2d(bunch.y[filter_not_nany]*1e6, bunch.yp[filter_not_nany]*1e9, bins=nbins)
                    H = np.rot90(H)
                    H = np.flipud(H)
                    Hmasked = np.ma.masked_where(H==0,H) 
                    plt.pcolormesh(xedges,yedges,Hmasked)
                    cbar = plt.colorbar()  
                    plt.xlabel('y [um]')
                    plt.ylabel('yp [nrad]')
                    plt.title('Turn '+str(i_turn))
                    plt.savefig(self.fig_directory+'phasespaces_yyp/phase_space_'+str(i_turn)+'.png', 
                        bbox_inches = 'tight')
                # grp=bunch_profile.create_group('yyp')
                # grp.create_dataset('xedges',data=xedges, compression="gzip", compression_opts=9)
                # grp.create_dataset('yedges',data=yedges, compression="gzip", compression_opts=9)
                # grp.create_dataset('H',data=H, compression="gzip", compression_opts=9)

                # x profile
                plt.clf()
                if len(filter_not_nan) > 0:
                    hist_x, bin_edges_x = np.histogram(bunch.x, bins=self.n_slices, 
                                                   range=(np.min(bunch.x),np.max(bunch.x)))
                    bin_centers_x = (bin_edges_x[:-1]+bin_edges_x[1:])/2
                    plt.plot(bin_centers_x*1e6, hist_x)
                    plt.xlabel('x [um]')
                    plt.ylabel('x profile [# macroparticles]')
                    plt.title('Turn '+str(i_turn))
                    plt.savefig(self.fig_directory+'profile_x/profile_x_'+str(i_turn)+'.png', 
                        bbox_inches = 'tight')
                    # dset = bunch_profile.create_dataset('prof_x', data=np.stack((bin_centers_x, hist_x)), compression="gzip", compression_opts=9)                
                # y profile
                plt.clf()
                if len(filter_not_nany) > 0:
                    hist_y, bin_edges_y = np.histogram(bunch.y, bins=self.n_slices, 
                                               range=(np.min(bunch.y),np.max(bunch.y)))
                    bin_centers_y = (bin_edges_y[:-1]+bin_edges_y[1:])/2
                    plt.plot(bin_centers_y*1e6, hist_y)
                    plt.xlabel('y [um]')
                    plt.ylabel('y profile [# macroparticles]')
                    plt.title('Turn '+str(i_turn))
                    plt.savefig(self.fig_directory+'profile_y/profile_y_'+str(i_turn)+'.png', bbox_inches = 'tight')
                    # dset = bunch_profile.create_dataset('prof_y', data=np.stack((bin_centers_y, hist_y)), compression="gzip", compression_opts=9)
                # z profile
                plt.clf()
                if len(filter_not_nany) > 0:
                    hist_z, bin_edges_z = np.histogram(bunch.z, bins=self.n_slices, 
                                               range=(np.min(bunch.z),np.max(bunch.z)))
                    bin_centers_z = (bin_edges_z[:-1]+bin_edges_z[1:])/2
                    plt.plot(bin_centers_z*1e6, hist_z)
                    plt.axvline(x=0, color='r', linestyle='--')
                    plt.xlabel('z [um]')
                    plt.ylabel('z profile [# macroparticles]')
                    plt.title('Turn '+str(i_turn))
                    plt.savefig(self.fig_directory+'profile_z/profile_z_'+str(i_turn)+'.png', bbox_inches = 'tight')
                    # dset = bunch_profile.create_dataset('prof_z', data=np.stack((bin_centers_z, hist_z)), compression="gzip", compression_opts=9)
                    plt.close()
                # bunch_profile.close()