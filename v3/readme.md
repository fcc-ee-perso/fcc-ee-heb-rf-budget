# Version notes

Error spotted in the v2 version related to the total voltage calculated at extraction (target bunch length)
Added inputs as separate json files for the different modes

